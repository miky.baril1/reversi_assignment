using Microsoft.VisualStudio.TestTools.UnitTesting;
using Reversi;
namespace ReversiTests
{
    [TestClass]
    public class UnitTest1
    {
        [DataTestMethod]
        [DataRow(0,4,Disc.RED)]
        [DataRow(2,0,Disc.RED)]
        [DataRow(1,4,Disc.WHITE)]
        [DataRow(4,3,Disc.WHITE)]
        //placing 4 discs returns true since they're allowed
        public void PlaceDiscTestTrue(int row, int col, Disc toPlace)
        {
            Board pos = new Board(5,5);
            
            Assert.IsTrue(pos.PlaceDisc(row,col,toPlace));
        }
        [DataTestMethod]
        [DataRow(0,7,Disc.RED)]
        [DataRow(10,5,Disc.RED)]
        [DataRow(8,5,Disc.WHITE)]
        [DataRow(6,4,Disc.WHITE)]
        //placing 4 discs returns true since they're allowed
        public void PlaceDiscTestFalse(int row, int col, Disc toPlace)
        {
            Board pos = new Board(5,5);
            
            Assert.IsFalse(pos.PlaceDisc(row,col,toPlace));
        }

        [TestMethod]
        public void GetWinnerTest()
        {
            //more RED than WHITE so RED wins
            Board pos = new Board(5,5);
            
            pos.PlaceDisc(0,1,Disc.RED);
            Assert.AreEqual(Result.RED,pos.GetWinner());
            
        }
    }
}
