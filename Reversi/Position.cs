using System;

namespace Reversi
{
    public class Position
    {
        public Position(int userRow,int userColumn)
        {
           this.Row =userRow;
           this.Column = userColumn; 
        }
        public int Row {get;}
        
        public int Column {get;}
    }


}