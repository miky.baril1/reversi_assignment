﻿using System;

namespace Reversi
{
    class Program
    {
        static void Main(string[] args)
        {
            IPlayer human = new HumanPlayer(Disc.WHITE);
            IPlayer computer = new ComputerPlayer(Disc.RED);

            Game game = new Game(human,computer);

            game.Play();

        }
    }
}
