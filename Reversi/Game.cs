using System;

namespace Reversi
{
    class Game
    {
        IPlayer human;
        IPlayer computer;

        public Game(IPlayer human, IPlayer computer){
            this.human = human;
            this.computer = computer;
        }

        public void Play(){
            Console.WriteLine("Enter the size of your board");
            
            string input = Console.ReadLine();
            int size;
            while(!int.TryParse(input, out size))
            {
                Console.WriteLine("Please enter a number");
                input = Console.ReadLine();
  
            }
        
            Board board = new Board(size,size);
            Board copy = board.Copy(size,size);

            Console.WriteLine(board.ToString());

            while(copy.CheckFullBoard())
            {
                 Position human = this.human.ChooseMove(copy);
                 board.PlaceDisc(human.Row,human.Column,this.human.MyDisc);
                 Console.WriteLine(board.ToString());
                Console.WriteLine("------");
                Position computer = this.computer.ChooseMove(copy);
                 board.PlaceDisc(computer.Row,computer.Column,this.computer.MyDisc);
                 Console.WriteLine(board.ToString());
            }
            Console.WriteLine(copy.GetWinner());
        }
    }


}