using System;

namespace Reversi
{
    public class Board
    {
        public Disc [,] array;

        public Disc this[int i, int j] => array[i, j];

        public Board(int row,int column )
        {
            array = new Disc[row,column]; 
            for (int i = 0; i < this.array.GetLength(0); i++){
                for(int j = 0; j < this.array.GetLength(1); j++){
                    array[i,j] = Disc.EMPTY;
                }
            }
            array[0,0] = Disc.RED; 
            array[0,array.GetLength(1)-1] = Disc.RED; 
            array[array.GetLength(0)-1,0] = Disc.WHITE;
            array[array.GetLength(0)-1,array.GetLength(1)-1] = Disc.WHITE;
            
        }
        
        public bool PlaceDisc(int row, int column, Disc toPlace)
        {
            bool legal = IsLegal(row, column, toPlace);
           
           // bool occupied = IsOccupied(row,column);
            
            if(legal==true)
            {
                Console.WriteLine("The disc can be placed here");
                //array[row,column] = toPlace;
                return true;
            }else
            {
                return false;
            }
        }

        public bool IsLegal(int row, int column, Disc toPlace)
        {
           int checkTop = row - 1 < 0 ? 0 : row - 1;
           int checkBot = row + 1 > array.GetLength(0) - 1 ? array.GetLength(0) - 1 : row + 1;
           int checkLeft = column - 1 < 0 ? 0 : column - 1;
           int checkRight = column + 1 > array.GetLength(1) - 1 ? array.GetLength(1) - 1 : column + 1;        

           for(int i = checkTop; i <= checkBot; i++) 
           {
               for(int j = checkLeft; j<=checkRight; j++)
               {
                   if(array[i,j] == toPlace && array[i,j] != Disc.EMPTY)
                   {
                        array[row,column]= toPlace;
                        array[i,j] = toPlace;  
                        //return true;     
                   }
                   else{
                        return true;
                    }
               }
           }
           return false;
        }

        // public bool IsOccupied(int row, int column)
        // {
        //     if(array[row,column]!=Disc.EMPTY)
        //     {
        //         Console.WriteLine("This position is occupied");               
        //         return true;
        //     }
        //     else
        //     {
        //         return false;
        //     }
        // }
        public Result GetWinner()
        {
            int redCount = 0;
            int whiteCount = 0;
            foreach(Disc i in array){
                if(i==Disc.RED)
                {
                    redCount++;
                }
                else if(i==Disc.WHITE)
                {
                    whiteCount++;
                }
                else
                {
                    return Result.TIE;
                }
            }
            if(redCount<whiteCount)
            {
                return Result.WHITE;
            }
            else if(redCount>whiteCount)
            {
                return Result.RED;
            }
            else
            {
                return Result.TIE;
            }
        }
        public Board Copy(int row,int col)
        {
            Board newBoard = new Board(row,col);
            newBoard.array = new Disc[row,col];
            for (int i = 0; i < this.array.GetLength(0); i++){
                for(int j = 0; j < this.array.GetLength(1); j++){
                   
                    newBoard.array[i,j] = this.array[i,j];
                }
            }
            return newBoard;

        }
        public override String ToString()
        {
            string table = "";
            for (int i = 0; i < array.GetLength(0); i++){
                
                for (int j = 0; j < array.GetLength(1); j++){
                    
                    if(array[i,j]==Disc.RED)
                    {
                        table += "R ";
                    }
                    else if(array[i,j]==Disc.WHITE)
                    {
                        table += "W ";
                    }
                    else
                    {
                        table += "- ";
                    }
                }
                table += Environment.NewLine;
            }
            return table;
        }
        public bool CheckFullBoard()
        {
            int red = 0;
            int white = 0;
            int empty = 0;
            foreach (var item in array)
            {
                if(item== Disc.RED)
                {
                    red++;
                }
                else if(item == Disc.WHITE)
                {
                    white++;
                }
                else
                {
                    empty++;
                }
            }
            if(empty==0||red==0||white==0)
            {
                Console.WriteLine("Game Over.");
                return false;
            }
            else
            {
                return true;

            }
        }
       
    }
}