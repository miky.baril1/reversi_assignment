using System;

namespace Reversi
{
    public class HumanPlayer : IPlayer
    {
        public Disc MyDisc{get;}
        public HumanPlayer(Disc myDisc)
        {
            this.MyDisc = myDisc;
        }
        public Position ChooseMove(Board array)
        {
            if(!array.CheckFullBoard())
        {
            throw new InvalidOperationException("Cannot place at this position"); 
        }
            int column, row;
            do
            {
                Console.WriteLine("Enter the row to place your disc");
                row = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter the column to place your disc");
                column = Convert.ToInt32(Console.ReadLine());

            }while (!array.PlaceDisc(row,column,this.MyDisc));
            
            Position pos = new Position(row,column);
            return pos;
        }
    }
}