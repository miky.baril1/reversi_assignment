using System;

namespace Reversi
{
    public class ComputerPlayer : IPlayer
    {
        public Disc MyDisc{get;}
        public ComputerPlayer(Disc myDisc)
        {
            this.MyDisc = myDisc;
        }
        public Position ChooseMove(Board board)
        {
        Random ran = new Random();
        int row = 0;
        int column = 0; 
        
        while(!board.PlaceDisc(row,column,this.MyDisc))
            {
               row = ran.Next(board.array.GetLength(0));
               column = ran.Next(board.array.GetLength(1));
            }
            
        Console.WriteLine("Bots turn");
        Position chosen = new Position(row,column);
        return chosen;
    }
    }
} 