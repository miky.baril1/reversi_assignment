using System;

namespace Reversi
{
        public enum Disc
        {
            EMPTY,
            RED,
            WHITE,
        }

        public enum Result
        {
            RED,
            WHITE,
            TIE

        }
}