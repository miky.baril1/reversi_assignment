using System;

namespace Reversi
{
    public interface IPlayer
    {
        Disc MyDisc {get;}
        Position ChooseMove(Board array);
    }
}